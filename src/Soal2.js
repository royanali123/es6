/*
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName()


*/

/*
const fullName = 'Zell Liew'
const Zell = {
    fullName
}

console.log(Zell.fullName)
*/

const newFunction = (firstName,lastName) => {
    return{
        firstName,
        lastName,
        fullName:()=>{
            console.log(firstName+" "+lastName);
        }
      
    }
}
newFunction("Royan","Ali").fullName();